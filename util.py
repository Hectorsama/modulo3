from decimal import Decimal

class Metrics:

    '''
    Euclidean distance definition
    '''

    def euclidean(self, a, b):
        return (sum([abs(x-y) ** 2 for x, y in zip(tuple(a), tuple(b))]))**0.5

    '''
    manhattan distance definition
    '''

    def manhattan(self, a, b):
        return sum(abs(val1-val2) for val1, val2 in zip(a, b))

    '''
    aux definitions
    '''
    def __p_root(self, value, root):
        root_value = 1 / float(root)
        return round(Decimal(value) ** Decimal(root_value), 3)
    
    def __count_zeros_one(self, array):
        count = {item: array.count(item) for item in array}
        zeros = 0
        ones = 0
        if 1 in count:
            ones = count[1]
        if 0 in count:
            zeros = count[0]

        return (zeros, ones)

    '''
    minkowski distance definition
    '''

    def minkowski(self, x, y, p_value):
        if not len(x) == len(y):
            raise NameError('Length are not the same')
        return (self.__p_root(sum(pow(abs(a-b), p_value)
                                  for a, b in zip(x, y)), p_value))

    '''
    minkowski with weigth distance definition
    '''

    def minkowski_w(self, x, y, q, w):
        array = []
        final = []
        if not len(x) == len(y):
            raise NameError('Length are not the same')
        for a, b in zip(x, y):
            array.append(pow(abs(a-b), q))
        for num1, num2 in zip(w, array):
            final.append(num1 * num2)
        return self.__p_root(sum(final), q)

    '''
    binary simetric with weigth distance definition
    '''

    def binary_simetric(self, x, y):
        if not len(x) == len(y):
            raise NameError('Length are not the same')
        d_x = self.__count_zeros_one(x)
        d_y = self.__count_zeros_one(y)
        q, r, s, t = d_x[1]+d_y[1], d_x[1]+d_y[0], d_x[0]+d_y[1], d_x[0]+d_y[0]
        return (r+s)/(q+r+s+t)

    '''
    binary not simetric with weigth distance definition
    '''

    def binary_not_simetric(self, x, y):
        if not len(x) == len(y):
            raise NameError('Length are not the same')
        d_x = self.__count_zeros_one(x)
        d_y = self.__count_zeros_one(y)
        q, r, s = d_x[1]+d_y[1], d_x[1]+d_y[0], d_x[0]+d_y[1]
        return (r+s)/(q+r+s)
    '''
    nominals distance definition
    '''
    def nominals(self, x, y):
        l_x = len(x)
        l_y = len(y)
        if not l_x == l_y:
            raise NameError('Length are not the same')
        if not l_x == l_y:
            raise NameError('Length are not the same')

        c = set(x) & set(y)
        p = len(x)
        m = len(list(c))
        return (p-m)/p
    '''
    ordinals distance definition
    '''
    def ordinals(self, x):
        x = sorted(x)
        total = len(x)
        ordinal = []
        for index in range(1,total):
            ordinal.append((x[index]-1)/(total-1))
        return ordinal



    '''
    Groups
    '''
    def group_1(self):
        x1 = [i for i in range(10)]
        x2 = [2*i for i in range(10)]
        x3 = [i for i in range(10, 20, 1)]
        p_val = 1
        w = [1, 1, 1, 1, 1, 1, 1, 1, 1, 1]
        print(f'Euclidian', self.euclidean(x1, x2))
        print('Euclidian', self.euclidean(x2, x3))
        print('Euclidian', self.euclidean(x1, x3))
        print('manhattan', self.manhattan(x1, x2))
        print('manhattan', self.manhattan(x2, x3))
        print('manhattan', self.manhattan(x1, x3))
        print('minkowski', self.minkowski(x1, x2, p_val))
        print('minkowski', self.minkowski(x2, x3, p_val))
        print('minkowski', self.minkowski(x1, x3, p_val))
        print('minkowski_w', self.minkowski_w(x1, x2, p_val, w))
        print('minkowski_w', self.minkowski_w(x2, x3, p_val, w))
        print('minkowski_w', self.minkowski_w(x1, x3, p_val, w))


    def group_2(self):
        x1 = [1 if i % 2 == 0 else 0 for i in range(10)]
        x2 = [1 if i % 3 == 0 else 0 for i in range(0, 30, 3)]
        x3 = [1 if i % 4 == 0 else 0 for i in range(0, 40, 4)]
        x4 = [1 if i % 5 == 0 else 0 for i in range(0, 40, 4)]
        x5 = [1 if i % 3 == 0 else 0 for i in range(10)]
        print('binary_simetric', self.binary_simetric(x1, x2))
        print('binary_simetric', self.binary_simetric(x1, x3))
        print('binary_simetric', self.binary_simetric(x1, x4))
        print('binary_simetric', self.binary_simetric(x1, x5))
        print('binary_simetric', self.binary_simetric(x2, x3))
        print('binary_simetric', self.binary_simetric(x2, x4))
        print('binary_simetric', self.binary_simetric(x2, x4))

        print('binary_not_simetric', self.binary_not_simetric(x1, x2))
        print('binary_not_simetric', self.binary_not_simetric(x1, x3))
        print('binary_not_simetric', self.binary_not_simetric(x1, x4))
        print('binary_not_simetric', self.binary_not_simetric(x1, x5))
        print('binary_not_simetric', self.binary_not_simetric(x2, x3))
        print('binary_not_simetric', self.binary_not_simetric(x2, x4))
        print('binary_not_simetric', self.binary_not_simetric(x2, x4))

    def group_3(self):
        x1 = ['panda', 'rojo', 'lentes']
        x2 = ['panda', 'rojo', 'sin lentes']
        x3 = ['panda', 'azul', 'lentes']
        x4 = ['león', 'azul', 'lentes']
        x5 = ['perro', 'azul', 'lentes']
        x6 = ['lobo', 'gris', 'lentes']
        x7 = ['lobo', 'gris', 'sin lentes']
        print('nominal', self.nominals(x1, x2))
        print('nominal', self.nominals(x1, x3))
        print('nominal', self.nominals(x1, x4))
        print('nominal', self.nominals(x1, x5))
        print('nominal', self.nominals(x1, x6))
        print('nominal', self.nominals(x1, x7))
        print('nominal', self.nominals(x2, x3))
        print('nominal', self.nominals(x2, x4))
        print('nominal', self.nominals(x2, x5))
        print('nominal', self.nominals(x2, x6))
        print('nominal', self.nominals(x2, x7))
        print('nominal', self.nominals(x3, x4))
        print('nominal', self.nominals(x3, x5))
        print('nominal', self.nominals(x3, x6))
        print('nominal', self.nominals(x3, x7))
        print('nominal', self.nominals(x4, x5))
        print('nominal', self.nominals(x5, x6))
        print('nominal', self.nominals(x6, x7))

    def group_4(self):
        x1 = [i for i in range(5)]
        x2 = [i for i in range(10, 15, 1)]
        x3 = [i for i in range(5, 10, 1)]
        print('ordinal', self.ordinals(x1))
        print('ordinal', self.ordinals(x2))
        print('ordinal', self.ordinals(x3))
